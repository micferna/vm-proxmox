# generate_id_manager.py

import asyncio
import random
import proxmoxer

class GenerateIDManager:
    def __init__(self, api_manager):
        self.api_manager = api_manager

    async def generate_unique_vmid(self, node, min_vmid=400, max_vmid=410):
        loop = asyncio.get_running_loop()
        proxmox = await self.api_manager.get_proxmox_api()
        while True:
            vmid = random.randint(min_vmid, max_vmid)
            try:
                await loop.run_in_executor(None, lambda: proxmox.nodes(node).qemu(vmid).status.current.get())
            except proxmoxer.ResourceException:
                return vmid
